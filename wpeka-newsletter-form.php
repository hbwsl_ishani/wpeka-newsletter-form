<?php
/**
 * Plugin Name: WPeka Newsletter Form
 * Plugin URI: https://club.wpeka.com
 * Description: This plugin adds signup widget .
 * Version: 1.0.0
 * Author: WPeka
 * Author URI: https://club.wpeka.com
 */

add_action('admin_menu', 'wpeka_newsletter_plugin_menu');

function wpeka_newsletter_plugin_menu() {
	add_menu_page('WPeka Newsletter Settings', 'WPeka Newsletter Settings', 'administrator', 'wpeka-newsletter-settings', 'wpeka_newsletter_setting_page', 'dashicons-admin-generic');
}

function wpeka_newsletter_setting_page() { ?>
	<div class="wrap">
		<h2>Wpeka Newsletter plugin setting</h2>
		
		<form method="post" action="options.php">
		    <?php settings_fields( 'wpeka-newsletter-settings-group' ); ?>
		    <?php do_settings_sections( 'wpeka-newsletter-settings-group' ); ?>
		     <table class="form-table">
			<tr valign="top">
			<th scope="row">Wpeka Newsletter form content</th>        
			<td><textarea name="wpeka-newsletter-form-content" class="" cols="50" rows="15"><?php echo esc_attr( get_option('wpeka-newsletter-form-content') ); ?></textarea></td>
			</tr>
			 <tr><td colspan="2">Use shortcode [wpeka-newsletter-form]</td></tr>
		     </table>
		    
		    <?php submit_button(); ?>

		</form>
	</div>
<?php }

add_action( 'admin_init', 'wpeka_newsletter_plugin_settings' );

function wpeka_newsletter_plugin_settings() {
	register_setting( 'wpeka-newsletter-settings-group', 'wpeka-newsletter-form-content' );
}
function wpeka_newsletter_form_shortcode_content() {
  return get_option( 'wpeka-newsletter-form-content' );
}
add_shortcode( 'wpeka-newsletter-form', 'wpeka_newsletter_form_shortcode_content');
 ?>